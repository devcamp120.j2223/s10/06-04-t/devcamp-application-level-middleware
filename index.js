//khai báo thư viện express
const express = require('express');

//khởi tạo ứng dụng nodejs
const app = new express();

//khai báo port chạy nodejs
const port = 8000;

//subtask4: middleware log time mỗi request
app.use((request, response, next) => {
    console.log(new Date());

    next();
}, (request, response, next) => {
    console.log(`Method: ${request.method}`);

    next();   
}, (request, response, next) => {
    console.log(`Application-level-middleware`);

    next();    
})

/*
//subtask5: middleware log request method
app.use('/hello', (request, response, next) => {
    console.log(`Method: ${request.method}`);

    next();
})*/
/*
const helloMiddeware = (request, response, next) => {
    console.log(new Date());

    next();
}

app.get('/hello', helloMiddeware, (request, response) => {
    response.status(200).json({
        message: `Hello Devcamp 120`
    })   
})
*/
app.get('/hello', (request, response) => {
    response.status(200).json({
        message: `Hello Devcamp 120`
    })   
})

app.get('/', (request, response) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`);
    
    response.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()} năm ${today.getFullYear()}`
    })
})

app.listen(port, () => {
    console.log(`App chạy trên cổng ${port}`);
})